#pragma once

#include <stdint.h>

typedef enum {
  robot_Error_Ok,
  robot_Error_InvalidInstruction,
} robot_Error;

typedef enum {
  robot_Direction_North,
  robot_Direction_East,
  robot_Direction_South,
  robot_Direction_West,
} robot_Direction;

typedef struct {
  int32_t x;
  int32_t y;
} robot_Position;

typedef struct {
  const char* data;
  uint32_t data_len;
} robot_Instructions;

typedef struct {
  robot_Position position;
  robot_Direction direction;
} robot_Robot;

robot_Error robot_execute_instruction(robot_Robot* robot,
                                      robot_Instructions* instructions);
