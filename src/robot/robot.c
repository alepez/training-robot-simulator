#include "robot.h"

static robot_Direction robot_rotate_right(robot_Direction direction) {
  robot_Direction new_direction = direction;

  switch (direction) {
  case robot_Direction_North:
    new_direction = robot_Direction_East;
    break;
  case robot_Direction_East:
    new_direction = robot_Direction_South;
    break;
  case robot_Direction_South:
    new_direction = robot_Direction_West;
    break;
  case robot_Direction_West:
    new_direction = robot_Direction_North;
    break;
  default:
    /* Ignore */
    break;
  }

  return new_direction;
}

static robot_Direction robot_rotate_left(robot_Direction direction) {
  robot_Direction new_direction = direction;

  switch (direction) {
  case robot_Direction_North:
    new_direction = robot_Direction_West;
    break;
  case robot_Direction_West:
    new_direction = robot_Direction_South;
    break;
  case robot_Direction_South:
    new_direction = robot_Direction_East;
    break;
  case robot_Direction_East:
    new_direction = robot_Direction_North;
    break;
  default:
    /* Ignore */
    break;
  }

  return new_direction;
}

void robot_advance(robot_Robot* robot) {
  switch (robot->direction) {
  case robot_Direction_North:
    robot->position.y += 1;
    break;
  case robot_Direction_East:
    robot->position.x += 1;
    break;
  case robot_Direction_South:
    robot->position.y -= 1;
    break;
  case robot_Direction_West:
    robot->position.x -= 1;
    break;
  }
}

robot_Error robot_execute_instruction(robot_Robot* robot,
                                      robot_Instructions* instructions) {
  robot_Error err = robot_Error_Ok;

  for (uint32_t i = 0; i < instructions->data_len; ++i) {
    char instruction = instructions->data[i];

    switch (instruction) {
    case 'A':
      robot_advance(robot);
      break;
    case 'R':
      robot->direction = robot_rotate_right(robot->direction);
      break;
    case 'L':
      robot->direction = robot_rotate_left(robot->direction);
      break;
    default:
      err = robot_Error_InvalidInstruction;
      break;
    }
  }

  return err;
}
