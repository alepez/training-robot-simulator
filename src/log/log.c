#include "log.h"

#include <stdio.h>

static const char* PREFIX[] = {"INF", "WRN", "ERR"};

void log_write(log_level_t level, const char* msg) {
  printf("%s %s\n", PREFIX[level], msg);
}
