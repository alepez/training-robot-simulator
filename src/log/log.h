#ifndef H_5C4441D17D7D407B8B23C3F3E13EC837
#define H_5C4441D17D7D407B8B23C3F3E13EC837

typedef enum {
  LOG_LEVEL_INFO,
  LOG_LEVEL_WARN,
  LOG_LEVEL_ERR,
} log_level_t;

void log_write(log_level_t level, const char* msg);

#define log_info(msg) log_write(LOG_LEVEL_INFO, msg)
#define log_warn(msg) log_write(LOG_LEVEL_WARN, msg)
#define log_err(msg) log_write(LOG_LEVEL_ERR, msg)

#endif // H_5C4441D17D7D407B8B23C3F3E13EC837
