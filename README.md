# Robot Simulator


[![pipeline status](https://gitlab.com/alepez/training-robot-simulator/badges/main/pipeline.svg)](https://gitlab.com/alepez/training-robot-simulator/-/commits/main) 
[![coverage report](https://gitlab.com/alepez/training-robot-simulator/badges/main/coverage.svg)](https://gitlab.com/alepez/training-robot-simulator/-/commits/main) 

## Development

Set up the project by updating all the submodules recursively:

```shell
git submodule update --init --recursive
```