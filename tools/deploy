#!/usr/bin/env bash

set -e
set -x

upload_to_owncloud() {
  local distributable="${1}"
  local auth="${NEXTCLOUD_GITLAB_AUTH}"
  local owncloud_root="https://cloud.bluewind.it"
  local owncloud_dir="test_gitlab_ci_deploy"
  local filename="$(basename "${distributable}")"
  local url="${owncloud_root}/remote.php/webdav/${owncloud_dir}/${filename}"

  curl \
    -u "${auth}" \
    -X PUT \
    "${url}" \
    --data-binary @"${distributable}"
}

main() {
  local build_dir="${1:-build}"
  local file_to_deploy="${build_dir}/examples/example1/example1"

  test -e "${file_to_deploy}"

  local version="$(date +%Y-%m-%dT%H-%M-%S --utc)"
  local zip_path="${build_dir}/$(basename "${file_to_deploy}")-${version}.zip"

  zip -r "${zip_path}" "${file_to_deploy}"

  upload_to_owncloud "${zip_path}"
}

main $@
