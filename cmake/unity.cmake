function(add_unity_test test_name test_src)
    get_filename_component(test_src_absolute ${test_src} REALPATH)

    add_custom_command(OUTPUT ${test_name}_runner.c
            COMMAND
            ruby ${CMAKE_SOURCE_DIR}/deps/Unity/auto/generate_test_runner.rb
            ${CMAKE_SOURCE_DIR}/tests/project.yml
            ${test_src_absolute} ${test_name}_runner.c
            DEPENDS ${test_src})

    add_executable(${test_name}
            ${test_src}
            ${test_name}_runner.c
            )

    target_include_directories(${test_name}
            PRIVATE ${CMAKE_SOURCE_DIR}/src
            )

    target_link_libraries(${test_name} PRIVATE unity cmock)

    # Generate unity output file as a text report
    add_custom_command(OUTPUT ${test_name}_test_output.txt
            COMMAND
            ${test_name} > ${test_name}_test_output.txt
            DEPENDS ${test_src})

    # Convert unity text report to a junity xml report
    add_custom_command(OUTPUT ${test_name}_test_output.xml
            COMMAND
            ruby ${CMAKE_SOURCE_DIR}/deps/Unity/auto/parse_output.rb -xml ${test_name}_test_output.txt
            DEPENDS ${test_name}_test_output.txt)

    # Add this report as dependency of test_report target
    target_add_to_target(${test_name}_test_output.xml test_report)

    add_test(NAME ${test_name} COMMAND ${test_name})
endfunction()