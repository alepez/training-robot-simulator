#include <assert.h>
#include <malloc.h>
#include <robot/robot.h>
#include <stdio.h>
#include <string.h>

const char* direction_to_string(robot_Direction d) {
  static const char* table[] = {"North", "East", "South", "West"};
  return table[d % 4];
}

int main(int argc, const char* argv[]) {
  int x;
  int y;
  int d;
  char instructions_str[256];

  printf("x: ");
  scanf("%d", &x);
  printf("y: ");
  scanf("%d", &y);
  printf("direction [0: North] [1: East] [2: South] [3: West]: ");
  scanf("%d", &d);
  while ((getchar()) != '\n') {
  }
  printf("Instructions: ");
  fgets(instructions_str, 256, stdin);

  robot_Robot robot = {
      .position =
          {
              .x = x,
              .y = y,
          },
      .direction = d,
  };

  printf("Robot initial position: %d, %d, %s\n", //
         robot.position.x, robot.position.y,
         direction_to_string(robot.direction));

  printf("Instructions: %s\n", instructions_str);

  robot_Instructions instructions = {
      .data = instructions_str,
      .data_len = strlen(instructions_str),
  };

  robot_execute_instruction(&robot, &instructions);

  printf("Robot end position: %d, %d, %s\n", //
         robot.position.x, robot.position.y,
         direction_to_string(robot.direction));

  return 0;
}
