#include <robot/robot.h>
#include <stdlib.h>
#include <string.h>
#include <unity.h>

static void setup_test(robot_Robot* robot, robot_Instructions* instructions,
                       int32_t x, int32_t y, robot_Direction dir,
                       const char* instructions_str) {
  robot->position.x = x;
  robot->position.y = y;
  robot->direction = dir;
  instructions->data = instructions_str;
  instructions->data_len = strlen(instructions_str);
}

void test_robot_no_instructions(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.position.x, 7);
  TEST_ASSERT_EQUAL(robot.position.y, 3);
  TEST_ASSERT_EQUAL(robot.direction, robot_Direction_North);
}

void test_robot_advance_move_one(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "A");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.position.y, 4);
}

void test_robot_advance_twice_move_two(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "AA");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.position.y, 5);
}

void test_robot_right_change_direction(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "R");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.direction, robot_Direction_East);
}

void test_robot_left_change_direction(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "L");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.direction, robot_Direction_West);
}

void test_robot_advance_on_x_when_direction_east(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_East, "A");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.position.x, 8);
  TEST_ASSERT_EQUAL(robot.position.y, 3);
}

void test_robot_execute_RAALAL(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "RAALAL");

  robot_execute_instruction(&robot, &instructions);

  TEST_ASSERT_EQUAL(robot.position.x, 9);
  TEST_ASSERT_EQUAL(robot.position.y, 4);
  TEST_ASSERT_EQUAL(robot.direction, robot_Direction_West);
}

void test_robot_return_error_on_invalid_instruction(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "X");

  robot_Error err = robot_execute_instruction(&robot, &instructions);
  TEST_ASSERT_EQUAL(robot_Error_InvalidInstruction, err);
}

void test_robot_return_ok_on_valid_instruction(void) {
  robot_Robot robot;
  robot_Instructions instructions;
  setup_test(&robot, &instructions, 7, 3, robot_Direction_North, "A");

  robot_Error err = robot_execute_instruction(&robot, &instructions);
  TEST_ASSERT_EQUAL(robot_Error_Ok, err);
}

#define RANDOM_IN_RANGE(T, min, max) \
  (T)((T)(min) + (T)(rand() % ((T)(max) - (T)(min))))

void test_robot_fuzz_valid_inputs(void) {
  static const char ARL[] = {'A', 'R', 'L'};

  srand(0);

  robot_Direction directions[] = {robot_Direction_North, robot_Direction_West,
                                  robot_Direction_South, robot_Direction_East};

  int tests_count = 10000;

  for (int i = 0; i < tests_count; ++i) {
    int64_t position_x = RANDOM_IN_RANGE(int64_t, -2147483647, 2147483647);
    int64_t position_y = RANDOM_IN_RANGE(int64_t, -2147483647, 2147483647);
    robot_Direction direction = RANDOM_IN_RANGE(
        robot_Direction, robot_Direction_North, robot_Direction_West + 1);

    robot_Robot robot = {
        .position =
            {
                .x = (int32_t)position_x,
                .y = (int32_t)position_y,
            },
        .direction = direction,
    };

    robot_Instructions instructions;
    int64_t instructions_len = RANDOM_IN_RANGE(int64_t, 0, 1000);

    char* data = (char*)malloc(instructions_len);

    for (int i = 0; i < instructions_len; ++i) {
      data[i] = ARL[rand() % 3];
    }

    instructions.data_len = instructions_len;
    instructions.data = data;

    robot_Error err = robot_execute_instruction(&robot, &instructions);
    TEST_ASSERT_EQUAL(robot_Error_Ok, err);

    free(data);
  }
}